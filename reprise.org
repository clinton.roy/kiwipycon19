#+EMAIL: clinton.roy@gmail.com
#+AUTHOR: Clinton Roy
#+TITLE: The Packaging Lifecycle with Poetry
#+LATEX_HEADER: \institute[ANSTO]{Australian Nuclear Science and Technology Organisation\\The Australian Synchrotron}
#+DATE: +Kiwi PyCon X+ \linebreak Melbourne Python Charmers
#+OPTIONS: H:2 toc:t email:t
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [aspectratio=169]
#+LATEX_OPTIONS: [presentation]
#+LATEX_HEADER: \usepackage{enumerate}
#+LATEX_HEADER: \usepackage{fancyvrb}
#+LATEX_HEADER: \usepackage{keystroke}
#+LATEX_HEADER: \usepackage{tikz}
#+LATEX_HEADER: \usepackage[edges]{forest}
#+LATEX_HEADER: \usepackage{verbatimbox}
#+LATEX_HEADER: \usetikzlibrary{arrows,positioning,shapes}
#+LATEX_HEADER: \newcommand{\humaninput}[1]{\textit{#1}\Return}
#+LATEX_HEADER: \RecustomVerbatimEnvironment{Verbatim}{Verbatim}{frame=leftline, commandchars=\\\{\}}

#+BEAMER_FRAME_LEVEL: 2

# ABSTRACT: 
# Right now is an interesting time in the land of Python packaging;
# there are a lot of ideas flying around about the correct way to
# package, develop and publish your code. Poetry is one thread in this
# conversation.  Abstract This talk aims to look at some of the ways
# Poetry moves the conversation forward, learns from other packaging
# ecosystems, and where Python itself is going.

# We will be talking file formats, Python Enhancement Proposals (PEPs)
# to do with packaging, different packaging ideologies and looking at
# what we can learn from completely different languages.

# We’ll also be taking a look at the community’s approach to finding the
# way forward.
# thirty minutes talk, ten minutes q&a

* Where are We Now?

** Obligatory XKCD Comic
#+ATTR_LATEX: :height 0.8\paperheight
[[./python_environment.png]]


** Time == Now
 * virtual environments
 * =setup.py=
 * =requirements.txt=
 * centralised packaging index


** What are the Issues?
** What are the Issues with Virtual Environments ?
 * Multiple Python versions
 * Difficult to teach
 * Failure to activate

** What are the Issues with =setup.py=?
 * imperative, not declarative
  * makes it hard to reason about things without running code
  * running code is bad bad bad\pause
 * a lot of legacy, organic decisions that are hard to change without
   breaking backwards compatibility.
  * a lot of decisions only documented in code\pause
 * multiple config files, multiple tools


* Where is Python Going?

** From a Great Height
 * modernisation of build and publishing tools
 * Instead of breaking backwards compatibility, striking out on a new
   path, while learning from history

** From Not Such a Great Height
 * picking interchange formats, standards and APIs, rather than tools
  * e.g. PEP 517 -- A build-system independent format for source trees
  * allows multiple tools to flourish, without anointing one\pause
 * occasionally breaking backwards compatibility then reverting that change


** From a Little Too Close for Comfort
 * A new way to configure builds
 * An automatic virtual environment

** Configuring builds
 A new file: =pyproject.toml=
 * PEP 518 -- Specifying Minimum Build System Requirements for Python Projects
 * Everyone can use it (build tools, code linters, testers etc.)
 * A new format: =toml=
  * Looks like =ini=, structure of =json=


\begin{myverbbox}{\tomlverbatim}
[library]
address = "123 some road"

[[book]]
title = "cooking for humans"
published = "2010-05-01"
price = 17.99

[[book]]
title = "cooking for programmers"
published = "2038-01-19"
price = 100.20





\end{myverbbox}

\begin{myverbbox}{\jsonverbatim}
{
 "library": {
  "address": "123 some rd"
 },
 "book": [
  {
   "title": "cooking for humans",
   "published": "2010-05-01",
   "price": 17.99
  },
  {
   "title": "cooking for programmers",
   "published": "2020-01-01",
   "price": 100.2
  }
 ]
}
\end{myverbbox}

** 
:PROPERTIES:
:BEAMER_env: block
:END:
\begin{tikzpicture}
[node distance=2mm,
box/.style = {draw, rounded corners,
              rectangle, rectangle split, rectangle split parts=2}]
\node [box] (toml) {
      TOML
      \nodepart{second}
      \tomlverbatim
      };
\node [box, right=of toml, anchor=west] (json) {
      JSON
      \nodepart{second}
      \jsonverbatim
      };
\end{tikzpicture}

** Virtual Environments without activation - PEP 582
 * PEP 582 -- Python local packages directory
 * Have a =__pypackages__= directory in your project directory\pause
  * Under that, a directory for each Python version you support\pause
   * And under those, your dependencies\pause
 * No activation required!
 * similar to =node_modules= in JavaScript land

\begin{forest}
  for tree={%
    folder,
    grow'=0,
    edge+={rounded corners},
    inner sep = 1mm,
  }
  [example,
     [pyproject.toml]
     [\_\_pypackages\_\_,
      [2.7, [pycparser]],
      [3.7, [pycparser]]],
]
\end{forest}



* Introducing Poetry

** Introducing Poetry
 * Unofficially, one tool to do all Python packaging tasks: 
  * handle multiple python versions
  * build your package
  * handle dependencies
  * publish
 * Not dissimilar to Rust's Cargo
 * Not quite 1.0 yet
 * A lot of functionality just recently added.
 * Some of the website doc is a little out of date.
 * Basically one author.

** Installing Poetry
 * Poetry has dependencies
 \pause
 * Your project has dependencies
 \pause
 * Poetry has dependencies\pause\hspace{-10.16em}Your project has
   dependencies \pause (don't mix them)
 \pause
 * Poetry Python version != Your project Python version
 * I'm a fan of getting it from your distro...
 * I'm a fan of a =pip install --user poetry=
 * I'm *not* a fan of the =get-poetry.py= vector

** Basic Poetry Assumptions
1. There's a clear separation between libraries and applications.
   * Libraries are packaged as wheels, installed via pip (public)
   * Applications are packaged via containers, installed locally (private)\pause
2. One repository is one package

** Workflow
 Poetry is a command line tool with a bunch of sub-commands:
 * create
 * manage virtual environments
 * maintain
 * develop
 * publish

** Workflow - Create - Metadata
 * =new= / =init=

\begin{Verbatim}
$ \humaninput{poetry init}

This command will guide you through creating your pyproject.toml config.

Package name [tmp]:  \humaninput{example}
Version [0.1.0]: \humaninput{}
Description []:  \humaninput{an example package}
Author [Clinton Roy <clintonr@ansto.gov.au>, n to skip]:\humaninput{}
License []:  \humaninput{gpl-1.0}
Compatible Python versions [^2.7]:  \humaninput{3.7}
\end{Verbatim}

** Workflow - =init= - Dependencies

\begin{Verbatim}
Would you like to define your main
  dependencies interactively? (yes/no) [yes] \humaninput{}
You can specify a package in the following forms:
  - A single name (requests)
  - A name and a constraint (requests ^2.23.0)
  - A git url (https://github.com/sdispater/poetry.git)
  - A git url with a revision
      (https://github.com/sdispater/poetry.git@develop)
  - A file path (../my-package/my-package.whl)
  - A directory (../my-package/)
  - An url (https://example.com/packages/my-package-0.1.0.tar.gz)
\end{Verbatim}

** Workflow - =init= - Dependencies cont.

\begin{Verbatim}
Add a package: \humaninput{pycparser}
Found 4 packages matching pycparser

Enter package # to add, or the complete package name if it is not listed:
 [0] pycparser-fake-libc
 [1] pycparser
 [2] pycparser-plz-ignore
 [3] pycparserext
 > \humaninput{1}
Enter the version constraint to require
   (or leave blank to use the latest version):\humaninput{}
Using version ^2.19 for pycparser

Add a package:\humaninput{}
\end{Verbatim}

** Workflow =init= cont. - Generated =pyproject.toml=

\begin{Verbatim}
[tool.poetry]
name = "example"
version = "0.1.0"
description = "an example package"
authors = ["Clinton Roy <clintonr@ansto.gov.au>"]
license = "gpl-1.0"

[tool.poetry.dependencies]
python = "3.7"
pycparser = "^2.19"

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"
\end{Verbatim}

** Poetry Versioning :noexport:
Poetry has four ways of specifying dependency versions
 * direct - ="2.19"=
 * wildcard - ="2.19.*"=
 * caret - ="^2.19"=
 * tilde - ="~2.19"=


** Workflow - Environment
 * =env=
\begin{Verbatim}
$ \humaninput{poetry env use python3.7}
Creating virtualenv test-QJGvohHS-py3.7 in
  /home/clintonr/.cache/pypoetry/virtualenvs
Using virtualenv:
      /home/clintonr/.cache/pypoetry/virtualenvs/test-QJGvohHS-py3.7
\end{Verbatim}

 * =shell=
\begin{Verbatim}
$ \humaninput{poetry shell}
Spawning shell within
  /home/clintonr/.cache/pypoetry/virtualenvs/test-QJGvohHS-py3.7
clintonr@rassilon:~/test$ \humaninput{which python}
/home/clintonr/.cache/pypoetry/virtualenvs/test-QJGvohHS-py3.7/bin/python
\end{Verbatim}

** Workflow - Develop - =install=
 * =install=
\begin{Verbatim}
$ \humaninput{poetry install}
Updating dependencies
Resolving dependencies... (0.1s)

Writing lock file

Package operations: 1 install, 0 updates, 0 removals

  - Installing pycparser (2.19)
  - Installing example (0.1.0)
\end{Verbatim}

** Workflow - Develop - =run=
 * Changes to =pyproject.toml=
\begin{Verbatim}
[tool.poetry.scripts]
hello = "example:hello"
\end{Verbatim}

 * Code =src/example/__init__.py=:
\begin{Verbatim}
import sys
def hello():
    ...
\end{Verbatim}

 * Now we can run our local tool:
\begin{Verbatim}
$ \humaninput{poetry run hello}
Kia ora from...
~/.cache/pypoetry/virtualenvs/example-gZR6Kgq7-py3.7/bin/python running
~/projects/kiwipycon19/example/src/example/__init__.py
\end{Verbatim}

** Workflow - Develop - =poetry.lock=

\begin{Verbatim}
[[package]]
category = "main"
description = "C parser in Python"
name = "pycparser"
optional = false
python-versions = ">=2.7, !=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*"
version = "2.19"

[metadata]
content-hash = "f9d4719ebddd24b596a8...3c9a77f7195b58d599118e7d1f22a7a3"
python-versions = "^3.7"

[metadata.hashes]
pycparser = ["a988718abfad80b57acce...30876d27603738ac39f140993246b25b3"]
\end{Verbatim}


** Do I commit =poetry.lock= ?
\begin{center}
\tikzstyle{decision} = [diamond, draw, fill=blue!20,
    text width=4.5em, text badly centered, node distance=3cm, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=blue!20,
    text width=5em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

\begin{tikzpicture}[node distance = 2cm, auto]
    % Place nodes
    \node [decision] (decide) {is project a library?};
    \node [block, below right of=decide, node distance=3cm] (yes) {don't commit!};
    \node [block, below left of=decide, node distance=3cm] (no) {commit!};
    % Draw edges
    \path [line] (decide) -| node [near end, color=black] {yes} (yes);
    \path [line] (decide) -| node [near end, color=black] {no} (no);
\end{tikzpicture}
\end{center}



** Workflow - Maintain - search :noexport:
 * =search=
\begin{Verbatim}
$ \humaninput{poetry search lrparsing}

lrparsing (1.0.13)
 An LR(1) parser hiding behind a pythonic interface

\end{Verbatim}

** Workflow - Maintain - add

 * =add= / =remove= / =update=
\begin{Verbatim}
$ \humaninput{poetry add lrparsing}
Using version ^1.0.13 for lrparsing

Updating dependencies
Resolving dependencies... (1.1s)

Writing lock file

Package operations: 1 install, 0 updates, 0 removals

  - Installing lrparsing (1.0.13)

\end{Verbatim}

** Workflow - Maintain - add cont.
\begin{Verbatim}
$ \humaninput{poetry add lrparsing}
\end{Verbatim}
=pyproject.toml= changes:
\begin{Verbatim}
[tool.poetry.dependencies]
...
lrparsing = "^1.0.13"
\end{Verbatim}
\vspace{1em}
=poetry.lock= changes:
\begin{Verbatim}
[[package]]
category = "main"
description = "An LR(1) parser hiding behind a pythonic interface"
name = "lrparsing"
optional = false
python-versions = "*"
version = "1.0.13"
\end{Verbatim}


** Workflow - Publish
 * =version= (bumping)
\begin{Verbatim}
$ \humaninput{poetry version minor}
Bumping version from 0.1.0 to 0.2.0
\end{Verbatim}

 * =build=
\begin{Verbatim}
$ \humaninput{poetry build}
Building example (0.2.0)
 - Building sdist
 - Built example-0.2.0.tar.gz

 - Building wheel
 - Built example-0.2.0-py3-none-any.whl
\end{Verbatim}

 * =publish=
 
** Workflow - Standard Command line stuff :noexport:
 * =about= / =help=
 * =debug=
 * =check=
 * =version=

** Workflow - Poetry specific :noexport:
 * =cache=
 * =config=
 * =export=
 * =self=


* Beyond Poetry :noexport:

** Jetty :noexport:
 * Firefox poetry based project for mono repos
 * no package metadata
 * no version bumping
 * API for cli commands

* What should You Do?

** What Can You Do?
 * try your own packages!
 * replace conditional code in =setup.py= with environment markers
 * like all new technology, trial it:
  * in a small corner
  * in a reversible way
  * be prepared to fail
 * submit bug reports
 * submit code changes

** Environment Markers

Change =setup.py= from this:

\begin{Verbatim}
install_requires = []
if sys.version_info < (3, 5):
    install_requires.append('typing >= 3.7.4')
\end{Verbatim}
\\
to this:
\begin{Verbatim}
install_requires = ["typing >= 3.7.4; python_version < '3.5'"]
\end{Verbatim}

as the latter can be parsed.

** Hell is Other Passive Aggressive People
 * your use case is special, not common.\pause
 * don't play the 'my big enterprise needs this bug fixed' card because:\pause
#+ATTR_LATEX: :options [a)]
      a. if your big enterprise needs it, it can pay for it\pause
      b. why is your big enterprise more important than a smaller company, or a hobbyist?\pause
      c. a big enterprise should not be deploying pre 1.0 software...\pause
 * be patient, have a look at the age of the other PRs before

* Thank You
** Thank You
To all the people who work on the ecosystem.


** Linux.Conf.Au
#+ATTR_LATEX: :width 0.6\textwidth :placement {l}{0.4\textwidth}
[[./lca2020_splash_board.jpg]]
 * Tickets are Available for Purchase
 * Call for Volunteers are Open
 * Call for Miniconf Presentations Open
