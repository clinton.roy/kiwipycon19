#!/usr/bin/python3.7

import json
import toml

INPUT = """
[library]
address = "123 some rd"

[[book]]
title = "cooking for humans"
published = "2010-05-01"
price = 17.99

[[book]]
title = "cooking for programmers"
published = "2020-01-01"
price = 100.20
"""

if __name__ == '__main__':
    d = toml.loads(INPUT)
    print(INPUT)
    print()
    print(json.dumps(d, indent=1))
